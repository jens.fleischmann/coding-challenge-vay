<?php
require_once __DIR__ . '/../index.php';

$config = $_GLOBAL['config'];

if (file_exists(__DIR__ . '/sql/_base.sql')) {
   $link = mysqli_connect($config['database']['host'], $config['database']['username'], $config['database']['passwd'], $config['database']['dbname']);

    if (!$link) {
        echo 'Error: Could not connect to MySQL/MariaDB.' . PHP_EOL;
        echo 'Error number: ' . mysqli_connect_errno() . PHP_EOL;
        echo 'Error message: ' . mysqli_connect_error() . PHP_EOL;
        exit;
    }

    $query = file_get_contents(__DIR__ . '/sql/_base.sql');
    $query = str_replace('%DBNAME%', $config['app']['dbname'], $query);
    echo $query . PHP_EOL;

    if (mysqli_multi_query($link, $query)) {
        do {
            /* store first result set */
            if ($result = mysqli_store_result($link)) {
                mysqli_free_result($result);
            }
        } while (mysqli_next_result($link));
    }

    mysqli_close($link);

    echo 'Database prepared' . PHP_EOL;
}
