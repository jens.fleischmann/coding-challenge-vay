CREATE DATABASE IF NOT EXISTS `%DBNAME%` CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `%DBNAME%`.`Company`;

CREATE TABLE IF NOT EXISTS `%DBNAME%`.`Company` (
  `company_code` varchar(10) DEFAULT NULL,
  `founder` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`company_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `%DBNAME%`.`Company` (`company_code`, `founder`) VALUES
('C1', 'Monika'),
('C2', 'Samantha'),
('C10', 'John');

DROP TABLE IF EXISTS `%DBNAME%`.`Lead_Manager`;

CREATE TABLE IF NOT EXISTS `%DBNAME%`.`Lead_Manager` (
  `lead_manager_code` varchar(10) DEFAULT NULL,
  `company_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`lead_manager_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `%DBNAME%`.`Lead_Manager` (`lead_manager_code`, `company_code`) VALUES
('LM1', 'C1'),
('LM2', 'C2'),
('LM3', 'C10'),
('LM4', 'C10');

DROP TABLE IF EXISTS `%DBNAME%`.`Senior_Manager`;

CREATE TABLE IF NOT EXISTS `%DBNAME%`.`Senior_Manager` (
  `senior_manager_code` varchar(10) DEFAULT NULL,
  `lead_manager_code` varchar(10) DEFAULT NULL,
  `company_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`senior_manager_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `%DBNAME%`.`Senior_Manager` (`senior_manager_code`, `lead_manager_code`, `company_code`) VALUES
('SM1', 'LM1', 'C1'),
('SM2', 'LM1', 'C1'),
('SM3', 'LM2', 'C2'),
('SM4', 'LM3', 'C10'),
('SM5', 'LM4', 'C10');

DROP TABLE IF EXISTS `%DBNAME%`.`Manager`;

CREATE TABLE IF NOT EXISTS `%DBNAME%`.`Manager` (
  `manager_code` varchar(10) DEFAULT NULL,
  `senior_manager_code` varchar(10) DEFAULT NULL,
  `lead_manager_code` varchar(10) DEFAULT NULL,
  `company_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`manager_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `%DBNAME%`.`Manager` (`manager_code`, `senior_manager_code`, `lead_manager_code`, `company_code`) VALUES
('M1', 'SM1', 'LM1', 'C1'),
('M2', 'SM3', 'LM2', 'C2'),
('M3', 'SM3', 'LM2', 'C2'),
('M4', 'SM4', 'LM3', 'C10'),
('M5', 'SM5', 'LM4', 'C10'),
('M6', 'SM5', 'LM4', 'C10');

DROP TABLE IF EXISTS `%DBNAME%`.`Employee`;

CREATE TABLE IF NOT EXISTS `%DBNAME%`.`Employee` (
  `employee_code` varchar(10) DEFAULT NULL,
  `manager_code` varchar(10) DEFAULT NULL,
  `senior_manager_code` varchar(10) DEFAULT NULL,
  `lead_manager_code` varchar(10) DEFAULT NULL,
  `company_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`employee_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `%DBNAME%`.`Employee` (`employee_code`, `manager_code`, `senior_manager_code`, `lead_manager_code`, `company_code`) VALUES
('E1', 'M1', 'SM1', 'LM1', 'C1'),
('E2', 'M2', 'SM3', 'LM2', 'C2'),
('E3', 'M2', 'SM3', 'LM2', 'C2'),
('E4', 'M3', 'SM3', 'LM2', 'C2'),
('E5', 'M4', 'SM4', 'LM3', 'C10'),
('E6', 'M5', 'SM5', 'LM4', 'C10'),
('E7', 'M5', 'SM5', 'LM4', 'C10'),
('E8', 'M6', 'SM5', 'LM4', 'C10');
