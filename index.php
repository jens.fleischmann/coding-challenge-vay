<?php
$_GLOBAL['config'] = include 'config/global.php';
if (file_exists('config/local.php')) {
    $_GLOBAL['config'] = array_replace_recursive($_GLOBAL['config'], (include 'config/local.php'));
}

/*
 * Start coding here
 */
