<?php
return [
    'database' => [
        'host' => 'localhost',
        'username' => 'root',
        'passwd' => '',
        'dbname' => 'mysql',
    ],
    'app' => [
        'dbname' => 'coding_challenge_vay',
    ]
];
