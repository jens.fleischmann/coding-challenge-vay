# Coding challange

## Requirements

* Lamp like stack with PHP and MySQL/MariaDB

## Installation

* Copy `config/local.php.dist` to `config/local.php` and add your system 
specific configuration using the same key structure like in 
`config/global.php`. Use a database user which has at least the following 
rights: `CREATE`, `DELETE`, `DROP`, `INSERT`, `SELECT`, `UPDATE`.
Hint: This challenge will use the database name which is set in the key 
`app.database`. Override this, if you need another database name.
* Execute `php setup/setup.php` to create the database with all needed data to
start the challenge.

## Challenge

* Write a SQL query to finish [https://www.hackerrank.com/challenges/the-company/problem](the Company Problem).
* Write a HTML page to output the Company Problem.  
* Normalize the database tables to remove the duplicate records and rewrite your SQL query if necessary.
* Write a formular page to insert new Companies, Lead Managers, Senior Managers, Managers and Employees.
